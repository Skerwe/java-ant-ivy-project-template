# Java Ant with Ivy Project Template

![Made with Java](https://forthebadge.com/images/badges/made-with-java.svg) ![Bitbucket Pipelines](https://img.shields.io/bitbucket/pipelines/Skerwe/java-ant-ivy-project-template?style=for-the-badge)

A Java project starter template using Apache Ant to build, test and package the application. Dependencies are managed with Apache Ivy.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

Make sure these are installed first.

- You will need Java 1.8 installed configured on your system PATH  
  It's best to use the [OpenJDK][openjdk], Windows users can get binaries from [AdoptOpenJDK][adoptopenjdk]
- [Apache Ant 1.10+](http://ant.apache.org/)
- [Apache Ivy 2.4+](http://ant.apache.org/ivy/)

### Installing

1. Clone the repository with the name of your new project:  
   `git clone https://Skerwe@bitbucket.org/indiesagtewerke/java-ant-ivy-project-template.git <project-name>`

2. Compile your application:  
   `ant compile`

3. Execute your application:  
   `ant run`

### Basic Build Targets

_`ant -projecthelp` will list all build targets_

- Clean generated build artifacts:  
  `ant clean`

- Compile source files:  
  `ant compile`

- Package application:  
  `ant dist`

- Run application:  
  `ant run`

- Test applicatio:  
  `ant test`

## License

This project is licensed under the MIT License -- see the [LICENSE](LICENSE) file for details

[openjdk]: https://openjdk.java.net/
[adoptopenjdk]: https://adoptopenjdk.net/
