package skerwe.hatenewt.template;

import skerwe.hatenewt.template.HelloWorld;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class HelloWorldTest {

    @Test
    public void testGetmessage() {
        HelloWorld messenger = new HelloWorld();
        assertEquals("Hello, world!", messenger.getMessage());
    }
}
