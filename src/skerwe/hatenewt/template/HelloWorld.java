package skerwe.hatenewt.template;

public class HelloWorld {

    public static void main(String[] args) {
        HelloWorld messenger = new HelloWorld();
        System.out.println(messenger.getMessage());
    }

    public String getMessage() {
        return "Hello, world!";
    }

}
